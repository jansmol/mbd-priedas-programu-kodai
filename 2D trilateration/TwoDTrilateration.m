%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%TwoDTrilateration.m failas skirtas trilateracijai atvaizduoti%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
%Prevents rounding when displaying fractions
format long
%Reader 2-D Coordinates [x, y]
transCoor1 = [1 16];
transCoor2 = [10 8];
transCoor3 = [0 0];
Reader = [1, 16; 10, 8; 0, 0];
%Plot reader locations
scatter(getcolumn(Reader(1:3,1:2),1),getcolumn(Reader(1:3,1:2),2), 'MarkerEdgeColor', 'blue', 'MarkerFaceColor', 'red');figure(gcf)
hold on
grid on;
Distance = [9.4; 7; 7.8];
%http://www.mathworks.com/matlabcentral/fileexchange/2876-draw-a-circle
circle(transCoor1,Distance(1), 1000,'blue');figure(gcf)
circle(transCoor2,Distance(2),1000, 'green');figure(gcf)
circle(transCoor3,Distance(3), 1000, 'magenta');figure(gcf)
pause;
x = 0;
y = 0;
%Reader(row, col)
%Calling two_tri_formula functon
[x, y] = two_tri_formula(Reader(1,1), Reader(2, 1), Reader(3,1), Reader(1, 2), Reader(2, 2), Reader(3, 2), Distance(1), Distance(2), Distance(3));
%plot found object
scatter(x,y, 'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor', [0 0 0]);figure(gcf)
circle([x, y],.5,1000,'-');figure(gcf)
legend('Fiksuot� pozicij� antenos ','Pirmos antenos diapazonas','Antros antenos diapazonas','Tre�ios antenos diapazonas','Apskai�iuota objekto pozicija');