////////////////////////////////////////////
//NRF24.c failas skirtas imtuvo irenginiui//
////////////////////////////////////////////
#include "NRF24.h"
//------------------------------------------------
extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart1;
//------------------------------------------------
#define TX_ADR_WIDTH 3
#define TX_PLOAD_WIDTH 7
#define TX_ADDRESS0_MSB 0xb3
#define TX_ADDRESS1_MSB 0xb7
#define TX_ADDRESS2 0xb6
uint8_t TX_ADDRESS0[TX_ADR_WIDTH] = {0xb3,0xb5,0x01};
uint8_t TX_ADDRESS1[TX_ADR_WIDTH] = {0xb7,0xb5,0xa1};
uint8_t TX_ADDRESS2_FULL[TX_ADR_WIDTH] = {0xb6,0xb5,0xa1};
uint8_t RX_BUF[TX_PLOAD_WIDTH+1] = {0};
volatile uint8_t rx_flag = 0, tx_flag = 0;
char str1[100];
char buffer[1] = {50};
uint8_t ErrCnt_Fl = 0;
extern uint32_t TIM1_Count;
extern uint8_t buf1[20];
uint16_t cnt1=0, cnt2=0, cnt3=0;
uint8_t line0[5] = {0x01, 0x01, 0x01, 0x01, 0x01};
uint8_t line1[5] = {0x02, 0x02, 0x02, 0x02, 0x02};
uint8_t line2[5] = {0x03, 0x03, 0x03, 0x03, 0x03};
int pipe_number = 99;
//------------------------------------------------
__STATIC_INLINE void DelayMicro(__IO uint32_t micros)
{
  micros *= (SystemCoreClock / 1000000) / 9;
  /* Wait till done */
  while (micros--) ;
}
//--------------------------------------------------
uint8_t NRF24_ReadReg(uint8_t addr)
{
  uint8_t dt=0, cmd;
  CS_ON;
  HAL_SPI_TransmitReceive(&hspi1,&addr,&dt,1,1000);
  if (addr!=STATUS)
  {
    cmd=0xFF;
    HAL_SPI_TransmitReceive(&hspi1,&cmd,&dt,1,1000);
  }
  CS_OFF;
  return dt;
}
//------------------------------------------------
void NRF24_WriteReg(uint8_t addr, uint8_t dt)
{
  addr |= W_REGISTER;
  CS_ON;
  HAL_SPI_Transmit(&hspi1,&addr,1,1000);
  HAL_SPI_Transmit(&hspi1,&dt,1,1000);
  CS_OFF;
}
//------------------------------------------------
void NRF24_ToggleFeatures(void)
{
  uint8_t dt[1] = {ACTIVATE};
  CS_ON;
  HAL_SPI_Transmit(&hspi1,dt,1,1000);
  DelayMicro(1);
  dt[0] = 0x73;
  HAL_SPI_Transmit(&hspi1,dt,1,1000);
  CS_OFF;
}
//-----------------------------------------------
void NRF24_Read_Buf(uint8_t addr,uint8_t *pBuf,uint8_t bytes)
{
  CS_ON;
  HAL_SPI_Transmit(&hspi1,&addr,1,1000);
  HAL_SPI_Receive(&hspi1,pBuf,bytes,1000);
  CS_OFF;
}
//------------------------------------------------
void NRF24_Write_Buf(uint8_t addr,uint8_t *pBuf,uint8_t bytes)
{
  addr |= W_REGISTER;
  CS_ON;
  HAL_SPI_Transmit(&hspi1,&addr,1,1000);
  DelayMicro(1);
  HAL_SPI_Transmit(&hspi1,pBuf,bytes,1000);
  CS_OFF;
}
//------------------------------------------------
void NRF24_FlushRX(void)
{
  uint8_t dt[1] = {FLUSH_RX};
  CS_ON;
  HAL_SPI_Transmit(&hspi1,dt,1,1000);
  DelayMicro(1);
  CS_OFF;
}
//------------------------------------------------
void NRF24_FlushTX(void)
{
  uint8_t dt[1] = {FLUSH_TX};
  CS_ON;
  HAL_SPI_Transmit(&hspi1,dt,1,1000);
  DelayMicro(1);
  CS_OFF;
}
//------------------------------------------------
void NRF24L01_RX_Mode(void)
{
  uint8_t regval=0x00;
  regval = NRF24_ReadReg(CONFIG);
  //Turn on RX mode
  regval |= (1<<PWR_UP)|(1<<PRIM_RX);
  NRF24_WriteReg(CONFIG,regval);
  CE_SET;
  DelayMicro(150); 
  // Flush buffers
  NRF24_FlushRX();
  NRF24_FlushTX();
}
//------------------------------------------------
void NRF24L01_TX_Mode(uint8_t TX_num, uint8_t *pBuf)
{
	  switch(TX_num)
  {
    case 0:
      NRF24_Write_Buf(TX_ADDR, TX_ADDRESS0, TX_ADR_WIDTH);
      NRF24_Write_Buf(RX_ADDR_P0, TX_ADDRESS0, TX_ADR_WIDTH);
      NRF24_Write_Buf(RX_ADDR_P1, TX_ADDRESS1, TX_ADR_WIDTH);
      NRF24_WriteReg(RX_ADDR_P2, TX_ADDRESS2);
      break;
    case 1:
      NRF24_Write_Buf(TX_ADDR, TX_ADDRESS1, TX_ADR_WIDTH);
      NRF24_Write_Buf(RX_ADDR_P0, TX_ADDRESS1, TX_ADR_WIDTH);
      NRF24_Write_Buf(RX_ADDR_P1, TX_ADDRESS0, TX_ADR_WIDTH);
      NRF24_WriteReg(RX_ADDR_P2, TX_ADDRESS2);
      break;
    case 2:
      NRF24_Write_Buf(TX_ADDR, TX_ADDRESS2_FULL, TX_ADR_WIDTH);
      NRF24_Write_Buf(RX_ADDR_P0, TX_ADDRESS2_FULL, TX_ADR_WIDTH);
      NRF24_Write_Buf(RX_ADDR_P1, TX_ADDRESS0, TX_ADR_WIDTH);
      NRF24_WriteReg(RX_ADDR_P2, TX_ADDRESS1_MSB);
      break;
    default:
      break;
  }
//  NRF24_Write_Buf(TX_ADDR, TX_ADDRESS0, TX_ADR_WIDTH);
  CE_RESET;
  // Flush buffers
  NRF24_FlushRX();
  NRF24_FlushTX();
}
//------------------------------------------------
void NRF24_Transmit(uint8_t addr,uint8_t *pBuf,uint8_t bytes)
{
  CE_RESET;
  CS_ON;
  HAL_SPI_Transmit(&hspi1,&addr,1,1000);
  DelayMicro(1);
  HAL_SPI_Transmit(&hspi1,pBuf,bytes,1000);
  CS_OFF;
  CE_SET;
}
//------------------------------------------------
uint8_t NRF24L01_Send(uint8_t TX_num, uint8_t *pBuf)
{
	uint8_t regval=0x00;
  NRF24L01_TX_Mode(TX_num, pBuf);
  regval = NRF24_ReadReg(CONFIG);
  //Turn on TX mode
  regval |= (1<<PWR_UP);
  regval &= ~(1<<PRIM_RX);
  NRF24_WriteReg(CONFIG,regval);
  DelayMicro(150); 
  NRF24_Transmit(WR_TX_PLOAD, pBuf, TX_PLOAD_WIDTH);
  CE_SET;
  DelayMicro(15);
  CE_RESET;
  return 0;
}
//------------------------------------------------
void NRF24L01_Receive(void)
{
	if(rx_flag==1)
	{
		sprintf(str1,"%5u  %5u  %5u Pipe number: %2u \r\n", *(int16_t*)RX_BUF, *(int16_t*)(RX_BUF+2), *(int16_t*)(RX_BUF+4), *(RX_BUF+7));
		HAL_UART_Transmit(&huart1,(uint8_t*)str1,strlen(str1),0x1000);		
		rx_flag = 0;
	}
}
//------------------------------------------------
void NRF24_ini(void)
{
	CE_RESET;
  DelayMicro(5000);
	NRF24_WriteReg(CONFIG, 0x0a); // Set PWR_UP bit, enable CRC(1 byte) &Prim_RX:0 (Transmitter)
  DelayMicro(5000);
	NRF24_WriteReg(EN_AA, 0x07); // Enable Pipe0, Pipe1 and Pipe2
	NRF24_WriteReg(EN_RXADDR, 0x07); // Enable Pipe0, Pipe1 and Pipe2
	NRF24_WriteReg(SETUP_AW, 0x01); // Setup address width=3 bytes
	NRF24_WriteReg(SETUP_RETR, 0x5F); // // 1500us, 15 retrans
	NRF24_ToggleFeatures();
	NRF24_WriteReg(FEATURE, 0);
	NRF24_WriteReg(DYNPD, 0);
	NRF24_WriteReg(STATUS, 0x70); //Reset flags for IRQ
	NRF24_WriteReg(RF_CH, 76); // Frequency 2476 MHz
	NRF24_WriteReg(RF_SETUP, 0x06); //TX_PWR:0dBm, Datarate:1Mbps
	NRF24_Write_Buf(TX_ADDR, TX_ADDRESS0, TX_ADR_WIDTH);
	NRF24_Write_Buf(RX_ADDR_P0, TX_ADDRESS0, TX_ADR_WIDTH);
	NRF24_Write_Buf(RX_ADDR_P1, TX_ADDRESS1, TX_ADR_WIDTH);
	NRF24_WriteReg(RX_ADDR_P2, TX_ADDRESS2);
	NRF24_WriteReg(RX_PW_P0, TX_PLOAD_WIDTH); //Number of bytes in RX payload in data pipe 0
	NRF24_WriteReg(RX_PW_P1, TX_PLOAD_WIDTH); //Number of bytes in RX payload in data pipe 1
	NRF24_WriteReg(RX_PW_P2, TX_PLOAD_WIDTH); //Number of bytes in RX payload in data pipe 2
  NRF24L01_RX_Mode();
  LED_OFF;
}
//--------------------------------------------------
void IRQ_Callback(void)
{
	uint8_t status=0x01;
	uint8_t pipe = 99;
	DelayMicro(10);
	status = NRF24_ReadReg(STATUS);
  if(status & 0x40)
  {
		LED_TGL;
		pipe = (status>>1)&0x07;
		NRF24_Read_Buf(RD_RX_PLOAD,RX_BUF,TX_PLOAD_WIDTH);
		*(RX_BUF+7) = pipe;
		pipe_number = pipe;
		NRF24_WriteReg(STATUS, 0x40);
		rx_flag = 1;
	}
	if(status&TX_DS) //tx_ds == 0x20
  {
    LED_TGL;
    NRF24_WriteReg(STATUS, 0x20);
		pipe_number = 99;
		NRF24_Write_Buf(TX_ADDR, TX_ADDRESS0, TX_ADR_WIDTH);
    NRF24_Write_Buf(RX_ADDR_P0, TX_ADDRESS0, TX_ADR_WIDTH);
    NRF24_Write_Buf(RX_ADDR_P1, TX_ADDRESS1, TX_ADR_WIDTH);
    NRF24_WriteReg(RX_ADDR_P2, TX_ADDRESS2);
    NRF24L01_RX_Mode();
  }
  else if(status&MAX_RT)
  {
		pipe_number = 99;
    NRF24_WriteReg(STATUS, 0x10);
    NRF24_FlushTX();
  }
}
//--------------------------------------------------
void TIM1_Callback(void)
{
		
	if(TIM1_Count%1000==0)
  {
		NRF24L01_Receive();
		if (pipe_number == 0) // if data came from Pipe0, send data back to address number 0, data (line0)
			{
				NRF24L01_Send(0, line0);
				pipe_number = 99;
				HAL_Delay(1);
			}
		if (pipe_number == 1) // if data came from Pipe0, send data back to address number 1, data (line1)
			{
				NRF24L01_Send(1, line1);
				pipe_number = 99;
				HAL_Delay(1);
			}
		if (pipe_number == 2) // if data came from Pipe0, send data back to address number 2, data (line2)
			{
				NRF24L01_Send(2, line2);
				pipe_number = 99;
				HAL_Delay(1);
			}
    HAL_Delay(1);
  }
}
