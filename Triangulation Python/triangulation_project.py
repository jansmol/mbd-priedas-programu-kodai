###################################################################
##triangulation_project.m failas skirtas trianguliacijos sistemai##
###################################################################
import time
import math
import RPi.GPIO as GPIO

# setup which pins are which
servoPinA=13
servoPinC=11
TRIG = 18
ECHO = 24
TRIG1 = 8
ECHO1 = 7
MARGIN=0.8
MAXRANGE=150
MINANGLE=25
b=19.5
SENSOROFFSET=2.5
a=0.0
c=0.0
targetA=0
targetC=0
desiredPosition=0
# set the trigger pulse length and timeouts
pulsetrigger = 0.0001 # Trigger duration in seconds
timeout = 100        # Length of sm timeout
measuretimes = 3
sleeptime = 0.001
timeout = timeout*(1000/57/2)
print timeout

def motorA():
    global pwm
    GPIO.setup(servoPinA, GPIO.OUT)
    pwm=GPIO.PWM(servoPinA, 50)
    pwm.start(7)

def motorC():
    global pwm
    GPIO.setup(servoPinC, GPIO.OUT)
    pwm=GPIO.PWM(servoPinC, 50)
    pwm.start(7)

def setDirection(direction):
    global position
    position = direction
	#ivestas kampo laipsnis paverciamas i dutycycle
    DC=1./18.*(direction)+2
    pwm.ChangeDutyCycle(DC)
    time.sleep(1)
    
def configure(trigger, echo, trigger1, echo1):
    TRIG = trigger
    ECHO = echo
    TRIG1 = trigger1
    ECHO1 = echo1
    GPIO.setup(TRIG, GPIO.OUT)
    GPIO.setup(TRIG1, GPIO.OUT)
    GPIO.setup(ECHO, GPIO.IN)
    GPIO.setup(ECHO1, GPIO.IN)

def fire_trigger():
    # Set trigger high for 0.0001s then drop it low
    GPIO.output(TRIG, True)
    time.sleep(pulsetrigger)
    GPIO.output(TRIG, False)
    
def fire_trigger1():
    # Set trigger high for 0.0001s then drop it low
    GPIO.output(TRIG1, True)
    time.sleep(pulsetrigger)
    GPIO.output(TRIG1, False)

def wait_for_echo(desired_state):
    countdown = timeout
    while (GPIO.input(ECHO) != desired_state and countdown > 0):
        countdown = countdown - 1
    return (countdown > 0) # Return true if success, false if timeout

def wait_for_echo1(desired_state):
    countdown1 = timeout
    while (GPIO.input(ECHO1) != desired_state and countdown1 > 0):
        countdown1 = countdown1 - 1
    return (countdown1 > 0) # Return true if success, false if timeout

def measure_time():
    # Fire the trigger to set the whole thing in motion
    fire_trigger()
    # Check that the echo goes high....
    if wait_for_echo(1):
        # Start the timer and wait for the echo to go low
        echo_start = time.time()
        if wait_for_echo(0):
            # Stop the timer
            echo_end = time.time()
            return echo_end - echo_start
        else:
            # print "Timeout 2"
            return -1
    else:
        # print "Timeout 1"
        return -1
		
def measure_time1():
    # Fire the trigger to set the whole thing in motion
    fire_trigger1()
    # Check that the echo goes high....
    if wait_for_echo1(1):
        # Start the timer and wait for the echo to go low
        echo_start1 = time.time()
        if wait_for_echo1(0):
            # Stop the timer
            echo_end1 = time.time()
            return echo_end1 - echo_start1
        else:
            # print "Timeout 2"
            return -1
    else:
        # print "Timeout 1"
        return -1
    
def measure_average_time():
    count = 1
    total_time = 0
    while(count <= measuretimes):
        total_time = total_time + measure_time()
        time.sleep(0.01)
        count = count + 1
    return total_time / measuretimes
	
def measure_average_time1():
    count1 = 1
    total_time1 = 0
    while(count1 <= measuretimes):
        total_time1 = total_time1 + measure_time1()
        time.sleep(0.01)
        count1 = count1 + 1
    return total_time1 / measuretimes
        
def distance_cm():
    time = measure_average_time()
    if time < 0:
        return -1
    else:
        return int(time * (1000000 / 58))
		
def distance_cm1():
    time1 = measure_average_time1()
    if time1 < 0:
        return -1
    else:
        return int(time1 * (1000000 / 58))
 
if __name__ == "__main__":
    print "Starting ultrasound test"
    # Set up the GPIO board
    GPIO.setmode(GPIO.BOARD)
    motorC()
    setDirection(90)
    PositiontargetC = position
    motorA()
    setDirection(90)
    PositiontargetA = position
    # Tell the Pi which pins the ultrasound is on
    configure(TRIG, ECHO, TRIG1, ECHO1)

    try:
        while True:
            a = distance_cm()
            if a < 0:
                a = 151
            else:
		a = round(a, 5) - 3
		print ("Pirmas sensorius %.5f cm " % a)
            time.sleep(sleeptime)
			
	    c = distance_cm1()
            if c < 0:
                c = 151
            else:
		c = round(c, 5) - 3
		print ("Antras sensorius %.5f cm " % c)
            time.sleep(sleeptime)
            if (a > MAXRANGE and c > MAXRANGE):
                targetC = 90
                targetA = 90
                print "Objektas nerastas"
            else:
                if abs(a-c) > b*MARGIN:
                    #manoma kad aptikti du skirtingai objektai
                    if a < c:
                        print "Aptiktas objektas pries sensoriu C"
                        targetC = PositiontargetC
                        c = math.sqrt(a*a+b*b-2*a*b*math.cos(math.radians(targetC)))
                        targetA = math.acos((a*a-b*b-c*c)/(-2*c*b))
                        targetA = math.degrees(targetA)
                    else:
                        print "Aptiktas objektas pries sensoriu A"
                        targetA = PositiontargetA
                        a = math.sqrt(c*c+b*b-2*c*b*math.cos(math.radians(targetA)))
                        targetC = math.acos((c*c-a*a-b*b)/(-2*a*b))
                        targetC = math.degrees(targetC)                        
                else:
                    print "Objekto matomas abieju sensoriu"
                    targetC = math.acos((c*c-a*a-b*b)/(-2*a*b))
                    targetC = math.degrees(targetC)
                    targetA = math.acos((a*a-b*b-c*c)/(-2*c*b))
                    targetA = math.degrees(targetA)          
            print ("krastine c %.5f " % c)
            print ("krastine a %.5f " % a)
            print ("kampasC %.5f " % targetC)
            print ("kampasA %.5f " % targetA)
            targetC = 180 - targetC
            motorC()
            setDirection(targetC)
            PositiontargetC = 180 - position
            motorA()
            setDirection(targetA)
            PositiontargetA = position

    except KeyboardInterrupt:
        print "Stopping"
        GPIO.cleanup()
