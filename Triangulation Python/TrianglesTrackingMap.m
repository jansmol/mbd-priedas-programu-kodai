%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%TrianglesTrackingMap.m failas skirtas trianguliacijai atvaizduoti%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
%triangle a side (data from python results)
a = [0, -30.92, 0, -32, 0, -37.183, 0, -35];
%triangle C angle (data from python results)
Cangle = [0, 180-50.90614, 0, 180-30.05044, 0, 180-35.07245, 0, 180-35.07245];
Cx(1:size(a)) = 0;
Cy(1:size(Cangle)) = 0;
%Plot lines as a line from sensor
Cxtotal = Cx + a .* cosd(Cangle);
Cytotal = Cy + a .* sind(Cangle);
hold on;
plot(Cxtotal, Cytotal, 'color','blue');

%triangle c side (data from python results)
c = [0, -24, 0, -18, 0, -24, 0, -22.09295];
%triangle A angle (data from python results)
Aangle = [0, 90, 0, 117.09610, 0, 117.09610, 0, 114.45189];
%19.5 distance between sensors
Ax(1:size(c)) = 19.5;
Ay(1:size(Aangle)) = 0;
%Plot lines as a line from sensor
Axtotal = Ax + c .* cosd(Aangle);
Aytotal = Ay + c .* sind(Aangle);
hold on;
plot(Axtotal, Aytotal, 'color','green');

%figure(2)
%objekto tracking from sensor C
aSekimas = [-30.92, -32, -37.183, -35];
CangleSekimas = [180-50.90614, 180-30.05044, 180-35.07245, 180-35.07245];
CxSekimas(1:size(aSekimas)) = 0;
CySekimas(1:size(CangleSekimas)) = 0;
CxArraySekimas = CxSekimas + aSekimas .* cosd(CangleSekimas);
CyArraySekimas = CySekimas + aSekimas .* sind(CangleSekimas);
hold on;
plot(CxArraySekimas, CyArraySekimas, '--','color','red');

%objekto tracking from sensor A
cSekimas = [-24, -18, -24, -22.09295];
AangleSekimas = [90, 117.09610, 117.09610, 114.45189];
AxSekimas(1:size(cSekimas)) = 19.5;
AySekimas(1:size(AangleSekimas)) = 0;
AxArraySekimas = AxSekimas + cSekimas .* cosd(AangleSekimas);
AyArraySekimas = AySekimas + cSekimas .* sind(AangleSekimas);
hold on;
plot(AxArraySekimas, AyArraySekimas,'bo','MarkerEdgeColor','r');

% Plot line (distance) between sensors
b = 19.5;
Bangle = 0;
Bx(1) = 0;
By(1) = 0;
Bx(2) = Bx(1) + b * cosd(Bangle);
By(2) = By(1) + b * sind(Bangle);
hold on;
plot(Bx, By, 'k-s','MarkerEdgeColor','k');

legend('C Jutiklio atstumai iki objekto ','A Jutiklio atstumai iki objekto ','Objekto judejimas','Objekto pozicija','Atstumas tarp jutikliu');
xlim([-15 40]);
ylim([-30 15]);
grid on;
